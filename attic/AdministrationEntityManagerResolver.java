package org.example.microrders.administration.control;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.deltaspike.data.api.EntityManagerResolver;

/**
 *
 * @author non
 */
public class AdministrationEntityManagerResolver implements EntityManagerResolver {

    @Inject 
    //@AdministrationDatabase
    private EntityManager em;
    
    @Override
    public EntityManager resolveEntityManager() {
        return em;
    }
    
}
