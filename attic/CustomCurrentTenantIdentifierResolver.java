package org.example.microrders;

import org.example.microrders.administration.control.TenantHolder;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

/**
 *
 * @author markus-lauer
 */
public class CustomCurrentTenantIdentifierResolver implements CurrentTenantIdentifierResolver {

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantName = TenantHolder.getCurrentTenant();
        System.err.println("-- tenantResolver: " + tenantName);
        return tenantName;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
    
}
