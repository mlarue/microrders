package org.example.microrders;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import org.example.microrders.administration.entity.Tenant;
import org.flywaydb.core.Flyway;
import org.hibernate.boot.Metadata;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

/**
 *
 * @author non
 */
public class HibernateIntegrator implements Integrator {

    @Override
    public void integrate(Metadata mtdt, SessionFactoryImplementor sfi, SessionFactoryServiceRegistry sfsr) {
        String unitName = sfi.getProperties().getProperty("hibernate.ejb.persistenceUnitName");
        Flyway flyway = new Flyway();
        
        Object dataSource = sfi.getProperties().get("hibernate.connection.datasource");
        if (dataSource != null) {
            System.err.println("integrate: unit: " + unitName + ", dataSource: " + dataSource);
            if ( dataSource instanceof DataSource) {
                flyway.setDataSource((DataSource)dataSource);
            }
            else {
                System.err.println("warning: dataSource is not a DataSource. skipping migration");
                return;
            }
        }
        else {
            System.err.println("integrate: unit: " + unitName + ", local");
            flyway.setDataSource(
                    sfi.getProperties().getProperty("hibernate.connection.url"),
                    sfi.getProperties().getProperty("hibernate.connection.username"),
                    sfi.getProperties().getProperty("hibernate.connection.password"));
        }
        
        if ("microrders-administration".equals(unitName) || "test-administration".equals(unitName)) {
            flyway.setLocations("classpath:db/migrations/administration");
            flyway.setSchemas("administration");
            flyway.migrate();
        }
        else {
//            EntityManager em = null;
//            if ( "microrders-domain".equals(unitName)) {
//                em = Persistence.createEntityManagerFactory("microrders-administration").createEntityManager();
//            }
//            else if ("test-domain".equals(unitName)) {
//                em = Persistence.createEntityManagerFactory("test-administration").createEntityManager();
//            }
//            
//            List<Tenant> tenants = em.createQuery("SELECT t FROM Tenant t", Tenant.class)
//                    .getResultList();
//
//            for(Tenant tenant : tenants) {
//                flyway.setLocations("classpath:db/migrations/domain");
//                flyway.setSchemas(tenant.getName());
//                flyway.migrate();
//            }
        }
        
    }

    @Override
    public void disintegrate(SessionFactoryImplementor sfi, SessionFactoryServiceRegistry sfsr) {
        System.err.println("disintegrate");
    }

}
