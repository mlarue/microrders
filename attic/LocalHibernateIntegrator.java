package org.example.microrders;

import org.flywaydb.core.Flyway;
import org.hibernate.boot.Metadata;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

/**
 *
 * @author non
 */
public class LocalHibernateIntegrator implements Integrator {

    @Override
    public void integrate(Metadata mtdt, SessionFactoryImplementor sfi, SessionFactoryServiceRegistry sfsr) {
        String unitName = sfi.getProperties().getProperty("hibernate.ejb.persistenceUnitName");
        Flyway flyway = new LocalFlywayProducer().createFlyway();
        System.err.println("integrate: unit: " + unitName + ", flyway: " + flyway);

        if ("test-administration".equals(unitName)) {
            flyway.setLocations("classpath:db/migrations/administration");
            flyway.setSchemas("administration");
            flyway.clean();
            flyway.migrate();
        }
        else if ("test-domain".equals(unitName)) {
//            flyway.setLocations("classpath:db/migrations/domain");
//            flyway.setSchemas("administration");
        }
    }

    @Override
    public void disintegrate(SessionFactoryImplementor sfi, SessionFactoryServiceRegistry sfsr) {
        System.err.println("disintegrate");
    }

}
