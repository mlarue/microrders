package org.example.microrders;

import java.util.HashMap;
import java.util.Map;
import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.engine.jdbc.connections.internal.DatasourceConnectionProviderImpl;
import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

/**
 *
 * @author markus-lauer
 */
public class LocalMultiTenantConnectionProvider extends AbstractMultiTenantConnectionProvider {

    final private Map<String, ConnectionProvider> connectionProviders = new HashMap<>();

    public LocalMultiTenantConnectionProvider() {
//        Map<String, String> properties = new HashMap<>();
//        properties.put("javax.persistence.jdbc.url", "jdbc:h2:./workshops;create=true");

        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:./workshops;create=true");
        //dataSource.setUser("");
        
        DatasourceConnectionProviderImpl connectionProvider = new DatasourceConnectionProviderImpl();
        connectionProvider.setDataSource(dataSource);
        //connectionProvider.configure(properties);
        
        //connectionProvider.
        
        connectionProviders.put("hans", connectionProvider);
    }
    
    @Override
    protected ConnectionProvider getAnyConnectionProvider() {
        System.err.println("-- getAnyConnectionProvider");
        //return connectionProviders.values().iterator().next();

        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:./workshops;create=true");
        //dataSource.setUser("");
        
        DatasourceConnectionProviderImpl connectionProvider = new DatasourceConnectionProviderImpl();
        connectionProvider.setDataSource(dataSource);
        
        return connectionProvider;
    }

    @Override
    protected ConnectionProvider selectConnectionProvider(String tenantIdentifier) {
        System.err.println("-- selectConnectionProvider: " + tenantIdentifier);
        return connectionProviders.get(tenantIdentifier);
    }

}
