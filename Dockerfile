FROM jboss/wildfly:10.1.0.Final

COPY ./h2console.war /opt/jboss/wildfly/standalone/deployments

RUN cd /opt/jboss/wildfly && ./bin/add-user.sh -u admin -p 123

COPY config.cli /opt/jboss/wildfly
RUN cd /opt/jboss/wildfly && ./bin/jboss-cli.sh --file=config.cli

COPY ./target/microrders.war /opt/jboss/wildfly/standalone/deployments

CMD ["/opt/jboss/wildfly/bin/standalone.sh","-b", "0.0.0.0","-bmanagement", "0.0.0.0"]