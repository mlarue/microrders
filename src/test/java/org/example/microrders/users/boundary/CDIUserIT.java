package org.example.microrders.users.boundary;

import org.example.microrders.administration.boundary.UserRepository;
import org.example.microrders.administration.boundary.TenantRepository;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.example.microrders.administration.entity.Tenant;
import org.example.microrders.TestUtil;
import org.example.microrders.administration.control.UserSession;
import org.example.microrders.administration.entity.Role;
import org.example.microrders.administration.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author markus-lauer
 */
@Ignore
@RunWith(CdiTestRunner.class)
//@TestControl(startScopes = { SessionScoped.class, ApplicationScoped.class})
public class CDIUserIT {

    @Inject
    EntityManager em;

    @Inject
    UserRepository userRepository;

    @Inject
    TenantRepository tenantRepository;

    @Inject
    UserSession userSession;

    @Inject
    TestUtil testUtil;

    @Before
    public void setup() {
        System.err.println("BEFORE --------------");

        Tenant root = tenantRepository.findByName("root");
        if (root == null) {
            root = new Tenant();
            root.setName("root");
            tenantRepository.save(root);
        }
        userSession.setCurrentTenant(root);
    }

    @Test
    public void createUser() {
        User user = userRepository.findByName("hans");
        if (user==null) {
            user = new User();
            user.setName("hans");
            user.setPassword("123");
            user.addRole(Role.SUPERUSER);
            em.getTransaction().begin();
            userRepository.save(user);
            em.getTransaction().commit();
        }

        User user2 = userRepository.findByName("hans");
        Assert.assertNotNull(user2);
        System.out.println("roles: " + user2.getRoles());
        Assert.assertTrue(user2.hasRole(Role.SUPERUSER));
    }

    @Test
    public void modifyUser() {
        createUser();

        User user = userRepository.findByName("hans");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.hasRole(Role.SUPERUSER));

        testUtil.runInTransaction(() -> {
            user.delRole(Role.SUPERUSER);
        });

        Assert.assertFalse(user.hasRole(Role.SUPERUSER));

        System.out.println("HERE --------------");

        User user2 = userRepository.findByName("hans");
        Assert.assertNotNull(user2);
        System.out.println("roles: " + user2.getRoles());
        Assert.assertFalse(user2.hasRole(Role.SUPERUSER));
    }

}
