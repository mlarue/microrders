//package org.example.microrders.users.boundary;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import io.undertow.Undertow;
//import io.undertow.servlet.Servlets;
//import io.undertow.servlet.api.DeploymentInfo;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import javax.enterprise.context.SessionScoped;
//import javax.inject.Inject;
//import javax.servlet.http.HttpServletResponse;
//import javax.ws.rs.ApplicationPath;
//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.Application;
//import javax.ws.rs.core.Response;
//import org.apache.deltaspike.testcontrol.api.TestControl;
//import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
//import org.example.microrders.JAXRSConfiguration;
//import org.example.microrders.ObjectMapperProvider;
//import org.example.microrders.users.entity.User;
//import org.jboss.resteasy.core.Dispatcher;
//import org.jboss.resteasy.mock.MockDispatcherFactory;
//import org.jboss.resteasy.mock.MockHttpRequest;
//import org.jboss.resteasy.mock.MockHttpResponse;
//import org.jboss.resteasy.plugins.server.tjws.TJWSEmbeddedJaxrsServer;
//import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
//import org.jboss.resteasy.spi.ResteasyDeployment;
//import org.jboss.resteasy.test.TestPortProvider;
//import org.jboss.weld.environment.servlet.Listener;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
///**
// *
// * @author markus-lauer
// */
//@RunWith(CdiTestRunner.class)
//@TestControl(startScopes = SessionScoped.class)
//public class CDIUsersResourceIT {
//
//    private UndertowJaxrsServer undertowJaxrsServer;
//    private TJWSEmbeddedJaxrsServer server;
//    private Client client;
//    private WebTarget target;
//    private Dispatcher dispatcher;
//
//    @Inject
//    private CDIUserIT userIT;
//
//    @Inject
//    private UsersResource usersResource;
//
//    @Before
//    public void setup() {
//        dispatcher = MockDispatcherFactory.createDispatcher();
//        dispatcher.getRegistry().addSingletonResource(usersResource);
//        dispatcher.getProviderFactory().registerProvider(ObjectMapperProvider.class);
//
//        Undertow.Builder serverBuilder = Undertow.builder().addHttpListener(12345, "localhost");
//        undertowJaxrsServer = new UndertowJaxrsServer();
//        undertowJaxrsServer.start(serverBuilder);
//
//        ResteasyDeployment deployment = new ResteasyDeployment();
//        deployment.setInjectorFactoryClass("org.jboss.resteasy.cdi.CdiInjectorFactory");
//        deployment.setApplicationClass(JAXRSConfiguration.class.getName());
//        deployment.getActualResourceClasses().add(UsersResource.class);
//
//        DeploymentInfo deploymentInfo = undertowJaxrsServer.undertowDeployment(deployment, "resources")
//                .setClassLoader(this.getClass().getClassLoader())
//                .setContextPath("/microrders")
//                .setDeploymentName("My Application")
//                .addListeners(Servlets.listener(Listener.class));
//
//        undertowJaxrsServer.deploy(deploymentInfo);
//
//        userIT.setup();
//        userIT.createUser();
//        
//        client = ClientBuilder.newClient();
//        target = client.target("http://localhost:12345/microrders/resources/users");
//    }
//
//    @After
//    public void cleanup() {
//        if (undertowJaxrsServer != null) {
//            undertowJaxrsServer.stop();
//        }
//        if (client != null) {
//            client.close();
//        }
//        if (undertowJaxrsServer != null) {
//            undertowJaxrsServer.stop();
//        }
//    }
//
//    @Test
//    public void getUsers() throws Exception {
//        userIT.setup();
//        userIT.createUser();
//
//        Response response = target.request().get();
//        Assert.assertEquals(200, response.getStatus());
//
//        String body = response.readEntity(String.class);
//        System.out.println("body: " + body);
//
//        ObjectMapper om = new ObjectMapper();
//        List<User> users = om.readValue(body, new TypeReference<List<User>>() {});
//        System.out.println("users: " + users);
//    }
//
//    @Test
//    public void getUsers2() throws Exception {
//        userIT.createUser();
//
//        MockHttpRequest request = MockHttpRequest.get("/users");
//        MockHttpResponse response = new MockHttpResponse();
//        dispatcher.invoke(request, response);
//        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
//        String body = response.getContentAsString();
//        System.out.println("body: " + body);
//
//        ObjectMapper om = new ObjectMapper();
//        List<User> users = om.readValue(body, new TypeReference<List<User>>() {
//        });
//        System.out.println("users: " + users);
//    }
//
////    @Test
////    public void createOrder() throws Exception {
////
////        Order order = new Order();
////
////        MockHttpRequest request = MockHttpRequest.post("/order");
////        request.contentType(MediaType.APPLICATION_JSON);
////        request.content(Mock);
////        MockHttpResponse response = new MockHttpResponse();
////        dispatcher.invoke(request, response);
////        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
////    }
//}
