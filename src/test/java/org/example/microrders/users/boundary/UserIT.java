//package org.example.microrders.users.boundary;
//
//import org.example.microrders.administration.boundary.UserRepository;
//import org.example.microrders.administration.boundary.TenantRepository;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.EntityTransaction;
//import javax.persistence.Persistence;
//import org.example.microrders.administration.entity.Tenant;
//import org.example.microrders.users.control.UserSession;
//import org.example.microrders.administration.entity.Role;
//import org.example.microrders.administration.entity.User;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Ignore;
//import org.junit.Test;
//
///**
// *
// * @author markus-lauer
// */
//@Ignore
//public class UserIT {
//
//    EntityManagerFactory emf = Persistence.createEntityManagerFactory("integration-test");
//    EntityManager em;
//    EntityTransaction tx;
//    UserRepository userRepository;
//    TenantRepository tenantRepository;
//    UserSession userSession;
//    
//    @Before
//    public void setup() {
//        em = emf.createEntityManager();
//        tx = em.getTransaction();
//        
//        //tenantRepository = new TenantRepository();
//        //tenantRepository.em = em;
//        
//        Tenant root = new Tenant();
//        root.setName("root");
//        tenantRepository.save(root);
//
//        userSession = new UserSession();
//        userSession.setCurrentTenant(root);
//        
//        userRepository = new UserRepository();
////        userRepository.em = em;
////        userRepository.userSession = userSession;
//    }
//    
//    @Test
//    public void createUser() {
//        User user = new User();
//        user.setName("hans");
//        user.setPassword("123");
//        user.addRole(Role.SUPERUSER);
//        
//        tx.begin();
//        userRepository.createUser(user);
//        tx.commit();
//
//        User user2 = userRepository.getByName("hans");
//        Assert.assertNotNull(user2);
//        System.out.println("roles: " + user2.getRoles());
//        Assert.assertTrue(user2.hasRole(Role.SUPERUSER));
//    }
//
//    @Test
//    public void modifyUser() {
//        createUser();
//
//        tx.begin();
//        User user = userRepository.getByName("hans");
//        Assert.assertNotNull(user);
//        Assert.assertTrue(user.hasRole(Role.SUPERUSER));
//        user.delRole(Role.SUPERUSER);
//        Assert.assertFalse(user.hasRole(Role.SUPERUSER));
//        tx.commit();
//        
//        User user2 = userRepository.getByName("hans");
//        Assert.assertNotNull(user2);
//        System.out.println("roles: " + user2.getRoles());
//        Assert.assertFalse(user2.hasRole(Role.SUPERUSER));
//    }
//    
//}
