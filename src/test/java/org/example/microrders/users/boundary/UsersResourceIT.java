//package org.example.microrders.users.boundary;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import java.util.List;
//import javax.servlet.http.HttpServletResponse;
//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.Response;
//import org.example.microrders.ObjectMapperProvider;
//import org.example.microrders.users.entity.User;
//import org.jboss.resteasy.core.Dispatcher;
//import org.jboss.resteasy.mock.MockDispatcherFactory;
//import org.jboss.resteasy.mock.MockHttpRequest;
//import org.jboss.resteasy.mock.MockHttpResponse;
//import org.jboss.resteasy.plugins.server.tjws.TJWSEmbeddedJaxrsServer;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// *
// * @author markus-lauer
// */
//public class UsersResourceIT {
//
//    private TJWSEmbeddedJaxrsServer server;
//    private Client client;
//    private WebTarget target;
//    private UserIT userIT;
//    private Dispatcher dispatcher;
//
//    @Before
//    public void setup() {
//        userIT = new UserIT();
//        userIT.setup();
//        userIT.createUser();
//        UsersResource usersResource = new UsersResource();
//        usersResource.userRepository = userIT.userRepository;
//        
//        dispatcher = MockDispatcherFactory.createDispatcher();
//        dispatcher.getRegistry().addSingletonResource(usersResource);
//        dispatcher.getProviderFactory().registerProvider(ObjectMapperProvider.class);
//        
//        server = new TJWSEmbeddedJaxrsServer();
//        server.setPort(12345);
//        server.getDeployment().getActualProviderClasses().add(ObjectMapperProvider.class);
//        server.getDeployment().getResources().add(usersResource);
//        server.start();
//        
//        client = ClientBuilder.newClient();
//        target = client.target("http://localhost:12345/users");
//    }
//
//    @After
//    public void cleanup() {
//        server.stop();
//        client.close();
//    }
//
//    @Test
//    public void getUsers() throws Exception {
//        Response response = target.request().get();
//        Assert.assertEquals(200, response.getStatus());
//        
//        String body = response.readEntity(String.class);
//        System.out.println("body: " + body);
//        
//        ObjectMapper om = new ObjectMapper();
//        List<User> users = om.readValue(body, new TypeReference<List<User>>() {});
//        System.out.println("users: " + users);
//    }
//    
//    @Test
//    public void getUsers2() throws Exception {
//        MockHttpRequest request = MockHttpRequest.get("/users");
//        MockHttpResponse response = new MockHttpResponse();
//        dispatcher.invoke(request, response);
//        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
//        String body = response.getContentAsString();
//        System.out.println("body: " + body);
//
//        ObjectMapper om = new ObjectMapper();
//        List<User> users = om.readValue(body, new TypeReference<List<User>>() {});
//        System.out.println("users: " + users);
//    }
//
////    @Test
////    public void createOrder() throws Exception {
////
////        Order order = new Order();
////
////        MockHttpRequest request = MockHttpRequest.post("/order");
////        request.contentType(MediaType.APPLICATION_JSON);
////        request.content(Mock);
////        MockHttpResponse response = new MockHttpResponse();
////        dispatcher.invoke(request, response);
////        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
////    }
//
//}
