package org.example.microrders;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.Specializes;
import org.flywaydb.core.Flyway;

/**
 *
 * @author non
 */
@Specializes
public class LocalFlywayProducer extends FlywayProducer {
    
    @Produces
    @Override
    public Flyway createFlyway() {
        System.err.println("-- LOCAL FLYWAY");
        Flyway flyway = new Flyway();
        flyway.setDataSource("jdbc:h2:./workshops;create=true", null, null);
        return flyway;
    }
    
}
