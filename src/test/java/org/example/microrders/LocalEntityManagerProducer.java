package org.example.microrders;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.Specializes;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author non
 */
@ApplicationScoped
@Specializes
public class LocalEntityManagerProducer extends EntityManagerProducer {

    @Override
    @PostConstruct
    public void init() {
        System.err.println("-- init LocalEntityManagerProducer");
        System.err.println("-- cleaning and migrating administration schema");
        migrationManagement.cleanAndMigrateAdmin();
    }
    
    @Override
    protected EntityManagerFactory createEntityManagerFactory(String tenantName) {
        migrationManagement.cleanAndMigrateDomain(tenantName);
        return createEntityManagerFactory("pu-test", tenantName);
    }

    @Produces
    @RequestScoped
    //@TransactionScoped
    @Override
    public EntityManager create() {
        return super.create();
    }

    @Override
    public void close(@Disposes EntityManager em) {
        super.close(em);
    }
   
}
