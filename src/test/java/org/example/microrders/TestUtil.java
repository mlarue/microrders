package org.example.microrders;

import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author non
 */
public class TestUtil {

    @Inject
    EntityManager em;
    
    public void runInTransaction(Runnable runnable) {
        em.getTransaction().begin();
        runnable.run();
        em.getTransaction().commit();
    }
}
