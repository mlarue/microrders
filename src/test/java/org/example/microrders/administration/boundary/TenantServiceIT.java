package org.example.microrders.administration.boundary;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.example.microrders.administration.entity.Tenant;
import org.flywaydb.core.Flyway;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author markus-lauer
 */
@RunWith(CdiTestRunner.class)
public class TenantServiceIT {

    @Inject
    private TenantService tenantService;

    @Inject
    private TenantRepository tenantRepository;

    @Inject
    private Flyway flyway;
    
    @Inject
    private MetaRepository metaRepository;
    
    @Before
    public void setup() {
        flyway.setLocations("classpath:db/migrations/administration");
        flyway.setSchemas("administration");
        flyway.clean();
        flyway.migrate();
    }
    
    private boolean hasTable(List<Object[]> tables, String tableName) {
        for (Object[] table : tables) {
            if ( "order".equals(table[1])) {
                return true;
            }
        }
        return false;
    }
    
    @Test
    public void createTenant() {
        tenantService.createTenant("OLIVER");

        Tenant tenant = tenantRepository.findByName("OLIVER");
        assertNotNull(tenant);
        
        List<Object[]> tables = metaRepository.getTables("OLIVER");
        System.err.println("bla: " + tables.stream()
                        .map(oa -> Arrays.asList(oa))
                        .collect(Collectors.toList()));

        assertTrue(hasTable(tables, "order"));
        assertTrue(hasTable(tables, "schema_version"));
        
        // better alternative
        flyway.setLocations("classpath:db/migrations/domain");
        flyway.setSchemas("OLIVER");
        flyway.validate();
    }

    @Test
    public void removeTenant() {
        tenantService.createTenant("OLIVER");
        tenantService.removeTenant("OLIVER");
        
        Tenant tenant = tenantRepository.findByName("OLIVER");
        assertNull(tenant);
        
        List<Object[]> tables = metaRepository.getTables("OLIVER");
        assertTrue(tables.isEmpty());
    }
}
