package org.example.microrders.order.boundary;

import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.example.microrders.administration.boundary.TenantRepository;
import org.example.microrders.administration.boundary.TenantService;
import org.example.microrders.administration.control.MigrationManagement;
import org.example.microrders.administration.control.TenantHolder;
import org.example.microrders.administration.entity.Tenant;
import org.example.microrders.order.entity.Order;
import org.example.microrders.order.entity.OrderStatus;
import org.example.microrders.administration.control.InitialData;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author non
 */
@RunWith(CdiTestRunner.class)
public class OrderRepositoryIT {
   
    @Inject
    private OrderRepository orderRepository;
    
    @Inject
    private MigrationManagement migrationManagement;
    
    @Inject
    private TenantService tenantService;
    
    @Inject
    private TenantRepository tenantRepository;
    
    @Inject
    private EntityManager entityManager;
    
    @Inject
    private InitialData initialData;
    
    @Before
    //@Transactional
    public void setup() {
        System.err.println("### @Before");
//        migrationManagement.cleanAdmin();
//        migrationManagement.cleanDomain("ROOT");
//        initialData.init();
        
        TenantHolder.setTenant("ROOT");
        
        List<String> tenants = Arrays.asList("hans", "otto", "werner");
        tenants.forEach(tenant -> {
            tenantService.createTenant(tenant);
        });
        
        EntityTransaction tx = entityManager.getTransaction();
        
        // TODO: run in transaction
        //tx.begin();
        tenantService.removeTenant("otto");
        //tx.commit();
        
        System.out.println("tenants: " + tenantRepository.findAll());
        
        TenantHolder.cleanupTenant();
    }
    
    @Test
    public void createOrder() {
        TenantHolder.setTenant("hans");
        Order order = new Order();
        order.setOrderId("order32325");
        order.setStatus(OrderStatus.STARTED);
        orderRepository.save(order);
        List<Order> orders = orderRepository.findAll();
        System.out.println("orders: " + orders);
        assertEquals(1, orders.size());
        TenantHolder.cleanupTenant();
        
        TenantHolder.setTenant("werner");
        orders = orderRepository.findAll();
        System.out.println("orders: " + orders);
        assertEquals(0, orders.size());

        // access administration schema in tenant context
        List<Tenant> tenants = tenantRepository.findAll();
        System.out.println("tenants: " + tenants);
        assertEquals(4, tenants.size());

        TenantHolder.cleanupTenant();
    }
    
}
