//package org.example.microrders.order.boundary;
//
//import javax.servlet.http.HttpServletResponse;
//import org.jboss.resteasy.core.Dispatcher;
//import org.jboss.resteasy.mock.MockDispatcherFactory;
//import org.jboss.resteasy.mock.MockHttpRequest;
//import org.jboss.resteasy.mock.MockHttpResponse;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// *
// * @author markus-lauer
// */
//public class OrderResourceIT {
//
//    private Dispatcher dispatcher;
//    private OrderRepositoryIT orderRepositoryIT;
//
//    @Before
//    public void setup() {
//        orderRepositoryIT = new OrderRepositoryIT();
//        orderRepositoryIT.setup();
//        
//        dispatcher = MockDispatcherFactory.createDispatcher();
//        OrderResource orderResource = new OrderResource();
//        orderResource.orderRepository = orderRepositoryIT.orderRepository;
//        
//        dispatcher.getRegistry().addSingletonResource(orderResource);
//    }
//
//    @After
//    public void cleanup() {
//    }
//
//    @Test
//    public void getOrder() throws Exception {
//        MockHttpRequest request = MockHttpRequest.get("/order");
//        MockHttpResponse response = new MockHttpResponse();
//        dispatcher.invoke(request, response);
//        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
//    }
//
////    @Test
////    public void createOrder() throws Exception {
////        
////        Order order = new Order();
////        
////        
////        
////        MockHttpRequest request = MockHttpRequest.post("/order");
////        request.contentType(MediaType.APPLICATION_JSON);
////        request.content(Mock);
////        MockHttpResponse response = new MockHttpResponse();
////        dispatcher.invoke(request, response);
////        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
////    }
//
//}
