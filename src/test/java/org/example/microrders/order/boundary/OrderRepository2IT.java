//package org.example.microrders.order.boundary;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.EntityTransaction;
//import javax.persistence.Persistence;
//import org.example.microrders.administration.entity.Tenant;
//import org.example.microrders.order.entity.Order;
//import org.example.microrders.administration.boundary.TenantRepository;
//import org.example.microrders.users.control.UserSession;
//import org.junit.Before;
//import org.junit.Ignore;
//import org.junit.Test;
//
///**
// *
// * @author markus-lauer
// */
//@Ignore
//public class OrderRepository2IT {
//
//    EntityManagerFactory emf = Persistence.createEntityManagerFactory("integration-test");
//    EntityManager em;
//    EntityTransaction tx;
//    TenantRepository tenantRepository;
//    OrderRepository2 orderRepository;
//    UserSession userSession;
//    
//    @Before
//    public void setup() {
//        em = emf.createEntityManager();
//        tx = em.getTransaction();
//        
//        //tenantRepository = new TenantRepository();
//        //tenantRepository.em = em;
//        
//        Tenant root = new Tenant();
//        root.setName("root");
//        em.persist(root);
//        //tenantRepository.createTenant(root);
//
//        userSession = new UserSession();
//        userSession.setCurrentTenant(root);
//        
//        orderRepository = new OrderRepository2();
//        orderRepository.userSession = userSession;
//        orderRepository.em = Persistence.createEntityManagerFactory("integration-test").createEntityManager();
//    }
//    
//    @Test
//    public void createOrder() {
//        Order order = new Order();
//        order.setOrderId("123456");
//        
//        orderRepository.createOrder(order);
//    }
//    
//}
