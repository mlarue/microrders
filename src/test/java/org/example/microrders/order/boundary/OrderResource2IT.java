//package org.example.microrders.order.boundary;
//
//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.Response;
//import org.jboss.resteasy.plugins.server.tjws.TJWSEmbeddedJaxrsServer;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// *
// * @author markus-lauer
// */
//public class OrderResource2IT {
//
//    private TJWSEmbeddedJaxrsServer server;
//    private Client client;
//    private WebTarget target;
//    private OrderRepositoryIT orderRepositoryIT;
//    
//    @Before
//    public void setup() {
//        orderRepositoryIT = new OrderRepositoryIT();
//        orderRepositoryIT.setup();
//        OrderResource orderResource = new OrderResource();
//        orderResource.orderRepository = orderRepositoryIT.orderRepository;
//
//        server = new TJWSEmbeddedJaxrsServer();
//        server.setPort(12345);
//        server.getDeployment().getResources().add(orderResource);
//        server.start();
//        
//        client = ClientBuilder.newClient();
//        target = client.target("http://localhost:12345/order");
//    }
//
//    @After
//    public void cleanup() {
//        server.stop();
//        client.close();
//    }
//
//    @Test
//    public void getOrder() throws Exception {
//        Response response = target.request().get();
//        Assert.assertEquals(200, response.getStatus());
//    }
//
////    @Test
////    public void createOrder() throws Exception {
////
////        Order order = new Order();
////
////        MockHttpRequest request = MockHttpRequest.post("/order");
////        request.contentType(MediaType.APPLICATION_JSON);
////        request.content(Mock);
////        MockHttpResponse response = new MockHttpResponse();
////        dispatcher.invoke(request, response);
////        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
////    }
//
//}
