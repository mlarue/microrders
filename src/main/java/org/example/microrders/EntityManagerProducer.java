package org.example.microrders;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.example.microrders.administration.control.MigrationManagement;
import org.example.microrders.administration.control.TenantHolder;

/**
 *
 * @author non
 */
@ApplicationScoped
public class EntityManagerProducer {

    @Inject
    protected MigrationManagement migrationManagement;

    final private Map<String, EntityManagerFactory> emfs = new HashMap<>();

    @PostConstruct
    public void init() {
        System.err.println("-- init EntityManagerProducer");
        System.err.println("-- migrating administration schema");
        migrationManagement.migrateAdmin();
    }
    
    public EntityManagerFactory getEntityManagerFactory(String tenantName) {
        System.err.println("-- getEntityManagerFactory: tenantName=" + tenantName);
        if (!emfs.containsKey(tenantName)) {
            emfs.put(tenantName, createEntityManagerFactory(tenantName));
        }
        return emfs.get(tenantName);
    }

    protected EntityManagerFactory createEntityManagerFactory(String tenantName) {
        migrationManagement.migrateDomain(tenantName);
        return createEntityManagerFactory("pu-microrders", tenantName);
    }

    protected EntityManagerFactory createEntityManagerFactory(String unitName, String tenantName) {
        System.err.println("-- createEntityManagerFactory: unitName=" + unitName + ", tenantName=" + tenantName);
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.default_schema", "`" + tenantName + "`");
        return Persistence.createEntityManagerFactory(unitName, properties);
    }

    @Produces
    @RequestScoped
    //@TransactionScoped
    public EntityManager create() {
        System.err.println("-- createEntityManager hash");
        // create a @RequestScoped EntityManager proxy
        // - one Proxy/EntityManager-Map per request
        // - if tenant changes inside a request, a new EntityManager is created and stored in a map
        final Map<String, EntityManager> ems = new HashMap<>();
        return (EntityManager) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class<?>[]{EntityManager.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        ems.forEach((k,v) -> {
                            System.err.print("tenantName=" + k);
                            if (v.isOpen()) {
                                System.err.println(" closing");
                                v.close();
                            }
                            else {
                                System.err.println(" not open");
                            }
                        });
                        return null;
                    }
                            
                    String tenantName = TenantHolder.getCurrentTenant();
                    if (tenantName == null || tenantName.isEmpty()) {
                        tenantName = "ROOT";
                    }
                    System.err.println("-- createEntityManager (proxy) " + tenantName);
                    if (!ems.containsKey(tenantName)) {
                        System.err.println("--   EntityManager not found. creating new..");
                        ems.put(tenantName, getEntityManagerFactory(tenantName).createEntityManager());
                    }
                    EntityManager target = ems.get(tenantName);
                    // TODO: check
                    //target.joinTransaction();
                    return method.invoke(target, args);
                });
    }

    public void close(@Disposes EntityManager em) {
        System.err.println("-- disposing entityManager");
        em.close();
    }

}
