package org.example.microrders;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import org.flywaydb.core.Flyway;

/**
 *
 * @author non
 */
public class FlywayProducer {

    @Resource(lookup = "java:/jboss/datasources/ExampleDS")
    private DataSource datasource;

    @Produces
    public Flyway createFlyway() {
        return createFlyway(datasource);
    }

    public Flyway createFlyway(DataSource datasource) {
        System.err.println("-- CONTAINER FLYWAY");
        Flyway flyway = new Flyway();
        flyway.setDataSource(datasource);
        return flyway;
    }
}
