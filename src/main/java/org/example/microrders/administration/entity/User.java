package org.example.microrders.administration.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.example.microrders.BaseEntity;

/**
 *
 * @author non
 */
@Entity
@Table(name = "`user`", schema = "`administration`")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @JsonIgnore
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserRole> userRoles = new ArrayList<>();

    @NotNull
    @ManyToOne(optional = false)
    private Tenant tenant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        try {
            this.password = Base64.getEncoder().encodeToString(
                    MessageDigest.getInstance("SHA-384").digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Role> getRoles() {
        return userRoles.stream().map(ur -> ur.getRole()).collect(Collectors.toList());
    }

    public void addRole(Role role) {
        UserRole userRole = new UserRole(this, role);
        userRoles.add(userRole);
    }

    public boolean hasRole(Role role) {
        return userRoles.stream().anyMatch(ur -> ur.getRole().equals(role));
    }

    public void delRole(Role role) {
        userRoles.removeIf(ur -> ur.getRole().equals(role));
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Override
    public String toString() {
        return "User{"
                + "id=" + getId()
                + ", name=" + name
                + ", tenant=" + tenant
                + '}';
    }

}
