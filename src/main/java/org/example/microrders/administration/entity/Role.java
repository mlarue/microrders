package org.example.microrders.administration.entity;

/**
 *
 * @author non
 */
public enum Role {
    SUPERUSER, SERVICE
}
