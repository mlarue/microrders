package org.example.microrders.administration.boundary;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.example.microrders.administration.control.MigrationManagement;
import org.example.microrders.administration.entity.Tenant;

/**
 *
 * @author non
 */
@Stateless
public class TenantService {

    @Inject
    private TenantRepository tenantRepository;

    @Inject
    private MigrationManagement migrationManagement;

    public void createTenant(String name) {
        Tenant tenant = new Tenant();
        tenant.setName(name);
        tenantRepository.save(tenant);
        resetTenant(name);
    }
    
    public void resetTenant(String name) {
        migrationManagement.cleanAndMigrateDomain(name);
    }

    public void removeTenant(String name) {
        Tenant tenant = tenantRepository.findByName(name);
        if (tenant == null) {
            throw new RuntimeException("tenant not found");
        }

        migrationManagement.cleanDomain(name);
        tenantRepository.remove(tenant);
    }

}
