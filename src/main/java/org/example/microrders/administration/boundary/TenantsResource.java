package org.example.microrders.administration.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.example.microrders.administration.control.TenantInterceptor;
import org.example.microrders.administration.entity.Tenant;

/**
 *
 * @author non
 */
@Path("administration/tenants")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
@Interceptors(TenantInterceptor.class)
public class TenantsResource {

    @Inject
    private TenantRepository tenantRepository;

    @GET
    public List<Tenant> getTenants() {
        return tenantRepository.findAll();
    }
}
