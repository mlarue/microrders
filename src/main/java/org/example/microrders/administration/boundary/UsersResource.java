package org.example.microrders.administration.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.example.microrders.administration.entity.User;

/**
 *
 * @author non
 */
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class UsersResource {

    @Inject
    UserRepository userRepository;

    @GET
    @Path("asArray")
    public JsonArray getUsersAsJsonArray() {
        return users(userRepository.findAll());
    }

    @GET
    public List<User> getUsers2() {
        List<User> users = userRepository.findAllWithRoles();
        System.out.println("UsersResource: " + users);
        
        //users.forEach(u -> u.getRoles());
        return users;
    }

    private JsonArray users(List<User> users) {
        return users.stream()
                .map(u -> user(u))
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();
    }

    private JsonObject user(User user) {
        return Json.createObjectBuilder()
                //.add("test", "peter")
                .add("name", user.getName())
                .add("id", user.getId().toString())
                //                .add("roles", user.getRoles().stream()
                //                        .map(r -> r.toString())
                //                        .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add))
                .build();
    }
}
