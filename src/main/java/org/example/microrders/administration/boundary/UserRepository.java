package org.example.microrders.administration.boundary;

import java.util.List;
import java.util.UUID;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import org.example.microrders.administration.entity.User;

/**
 *
 * @author non
 */
//@Transactional
@Repository
//@EntityManagerConfig(entityManagerResolver = AdministrationEntityManagerResolver.class, flushMode = FlushModeType.COMMIT)
public interface UserRepository extends EntityRepository<User, UUID> {

    @Query("SELECT u FROM User u JOIN FETCH u.userRoles")
    public List<User> findAllWithRoles();
    
    @Query(singleResult = SingleResultType.OPTIONAL)
    public User findByName(String name);

}
