package org.example.microrders.administration.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author non
 */
@Stateless
public class MetaRepository {

    @Inject
    private EntityManager em;

    public List<Object[]> getTables(String schema) {
        return em.createNativeQuery(
                "SELECT table_schema,table_name "
                + "FROM information_schema.tables "
                + "WHERE table_schema=? "
                + "ORDER BY table_schema,table_name")
                .setParameter(1, schema)
                .getResultList();
    }

}
