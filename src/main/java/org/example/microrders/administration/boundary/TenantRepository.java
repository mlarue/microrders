package org.example.microrders.administration.boundary;

import java.util.UUID;
import javax.transaction.Transactional;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import org.example.microrders.administration.entity.Tenant;

/**
 *
 * @author non
 */
//@Transactional
@Repository
public interface TenantRepository extends EntityRepository<Tenant, UUID> {

    @Query(singleResult = SingleResultType.OPTIONAL)    
    public Tenant findByName(String name);
    
}
