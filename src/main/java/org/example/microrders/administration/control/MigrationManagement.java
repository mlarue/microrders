package org.example.microrders.administration.control;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import org.flywaydb.core.Flyway;

/**
 *
 * @author non
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class MigrationManagement {

    @Inject
    private Flyway flyway;

    public void migrateAdmin() {
        flyway.setLocations("classpath:db/migrations/administration");
        flyway.setSchemas("administration");
        flyway.migrate();
    }

    public void cleanAdmin() {
        flyway.setLocations("classpath:db/migrations/administration");
        flyway.setSchemas("administration");
        flyway.clean();
    }

    public void cleanAndMigrateAdmin() {
        flyway.setLocations("classpath:db/migrations/administration");
        flyway.setSchemas("administration");
        flyway.clean();
        flyway.migrate();
    }

    public void migrateDomain(String tenantName) {
        flyway.setLocations("classpath:db/migrations/domain");
        flyway.setSchemas(tenantName);
        flyway.migrate();
    }

    public void cleanDomain(String tenantName) {
        flyway.setLocations("classpath:db/migrations/domain");
        flyway.setSchemas(tenantName);
        flyway.clean();
    }

    public void cleanAndMigrateDomain(String tenantName) {
        flyway.setLocations("classpath:db/migrations/domain");
        flyway.setSchemas(tenantName);
        flyway.clean();
        flyway.migrate();
    }

}
