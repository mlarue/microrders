package org.example.microrders.administration.control;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.example.microrders.administration.boundary.TenantRepository;
import org.example.microrders.administration.boundary.UserRepository;
import org.example.microrders.administration.entity.Role;
import org.example.microrders.administration.entity.Tenant;
import org.example.microrders.administration.entity.User;

//@Stateless
//@Transactional
//@ApplicationScoped
@Singleton
@Startup
public class InitialData {

    @Inject
    private TenantRepository tenantRepository;

    @Inject
    private UserRepository userRepository;

    @PostConstruct
    public void init(/* @Observes @Initialized(ApplicationScoped.class) Object init */) {
        System.err.println("======== initialData");

        Tenant rootTenant = tenantRepository.findByName("ROOT");
        if (rootTenant == null) {
            rootTenant = new Tenant();
            rootTenant.setName("ROOT");
            tenantRepository.saveAndFlush(rootTenant);
        }

        User adminUser = userRepository.findByName("admin");
        if (adminUser == null) {
            adminUser = new User();
            adminUser.setName("admin");
            adminUser.setPassword("123");
            adminUser.addRole(Role.SUPERUSER);
            adminUser.setTenant(rootTenant);
            userRepository.saveAndFlush(adminUser);
        }
        
        Tenant fritzTenant = tenantRepository.findByName("FRITZ");
        if (fritzTenant == null) {
            fritzTenant = new Tenant();
            fritzTenant.setName("FRITZ");
            tenantRepository.saveAndFlush(fritzTenant);
        }

        User rudiUser = userRepository.findByName("rudi");
        if (rudiUser == null) {
            rudiUser = new User();
            rudiUser.setName("rudi");
            rudiUser.setPassword("123");
            rudiUser.addRole(Role.SUPERUSER);
            rudiUser.setTenant(fritzTenant);
            userRepository.saveAndFlush(rudiUser);
        }
    }
}
