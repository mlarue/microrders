package org.example.microrders.administration.control;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import org.example.microrders.administration.entity.Tenant;

/**
 *
 * @author non
 */
@SessionScoped
public class UserSession implements Serializable {
    
    private Tenant currentTenant;

    public Tenant getCurrentTenant() {
        return currentTenant;
    }

    public void setCurrentTenant(Tenant currentTenant) {
        this.currentTenant = currentTenant;
    }
    
}
