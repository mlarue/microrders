package org.example.microrders.administration.control;

/**
 *
 * @author non
 */
public class TenantHolder {

    private static final InheritableThreadLocal<String> CURRENT_TENANT = new InheritableThreadLocal<>();

    public static String getCurrentTenant() {
        return CURRENT_TENANT.get();
    }

    public static void setTenant(final String tenantName) {
        System.err.println("TenantHolder: setTenant: " + tenantName);
        CURRENT_TENANT.set(tenantName);
    }

    public static void cleanupTenant() {
        System.err.println("TenantHolder: cleanupTenant");
        CURRENT_TENANT.remove();
    }

}
