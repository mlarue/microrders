package org.example.microrders.administration.control;

import java.security.Principal;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;
import org.example.microrders.administration.boundary.UserRepository;
import org.example.microrders.administration.entity.User;

/**
 *
 * @author non
 */
public class TenantInterceptor {

    @Inject
    private HttpServletRequest httpServletRequest;

    @Inject
    private UserRepository userRepository;

    @AroundInvoke
    public Object wrapWithTenant(final InvocationContext ctx) throws Exception {
        Principal principal = httpServletRequest.getUserPrincipal();
        System.err.println("principal: " + principal.getName());
        User user = userRepository.findByName(principal.getName());
        System.err.println("user: " + user);

        final String oldValue = TenantHolder.getCurrentTenant();
        try {
            TenantHolder.setTenant(user.getTenant().getName());
            return ctx.proceed();
        } finally {
            if (oldValue != null) {
                TenantHolder.setTenant(oldValue);
            }
            else {
                TenantHolder.cleanupTenant();
            }
        }
    }

}
