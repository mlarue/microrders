package org.example.microrders.order.boundary;

import java.util.UUID;
import javax.transaction.Transactional;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.example.microrders.order.entity.Order;

/**
 *
 * @author non
 */
@Transactional
@Repository
public interface OrderRepository extends EntityRepository<Order, UUID> {

}
