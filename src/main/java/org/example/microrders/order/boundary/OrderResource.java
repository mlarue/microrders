//package org.example.microrders.order.boundary;
//
//import java.net.URI;
//import java.util.List;
//import java.util.UUID;
//import javax.ejb.Stateless;
//import javax.inject.Inject;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import org.example.microrders.order.entity.Order;
//
///**
// *
// * @author markus-lauer
// */
//@Stateless
//@Path("order")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class OrderResource {
//    
//    @Inject
//    OrderRepository2 orderRepository;
//    
//    @GET
//    public List<Order> getOrders() {
//        return orderRepository.getOrders();
//    }
//
//    @GET
//    @Path("{id}")
//    public Order getOrder(@PathParam("id") UUID id) {
//        return orderRepository.getOrder(id);
//    }
//    
//    @POST
//    public Response createOrder(Order order) {
//        orderRepository.createOrder(order);
//        return Response
//                .created(URI.create("/order/" + order.getId()))
//                .build();
//    }
//
//    @PUT
//    @Path("{id}")
//    public Response updateOrder(@PathParam("id") UUID id, Order order) {
//        orderRepository.updateOrder(order);
//        return Response.ok().build();
//    }
//    
//}
