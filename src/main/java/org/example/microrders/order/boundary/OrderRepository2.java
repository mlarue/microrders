//package org.example.microrders.order.boundary;
//
//import java.security.AccessControlException;
//import java.util.List;
//import java.util.UUID;
//import javax.ejb.Stateless;
//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import org.example.microrders.order.entity.Order;
//import org.example.microrders.order.entity.OrderStatus;
//import org.example.microrders.users.control.UserSession;
//
///**
// *
// * @author markus-lauer
// */
//@Stateless
//public class OrderRepository2 {
//    
//    @PersistenceContext
//    EntityManager em;
//    
//    @Inject
//    UserSession userSession;
//    
//    public List<Order> getOrders() {
//        return em.createQuery("SELECT o FROM Order o WHERE o.tenant=:tenant", Order.class)
//                .setParameter("tenant", userSession.getCurrentTenant())
//                .getResultList();
//    }
//
//    public Order getOrder(UUID id) {
//        Order order = em.find(Order.class, id);
////        if (!order.getTenant().equals(userSession.getCurrentTenant())) {
////            throw new AccessControlException("order belongs to another tenant");
////        }
//        return order;
//    }
//    
//    public void createOrder(Order order) {
//        order.setStatus(OrderStatus.CREATED);
////        order.setTenant(userSession.getCurrentTenant());
//        em.persist(order);
//    }
//
//    public void updateOrder(Order order) {
////        if (!order.getTenant().equals(userSession.getCurrentTenant())) {
////            throw new AccessControlException("order belongs to another tenant");
////        }
//        em.merge(order);
//    }
//    
//}
