package org.example.microrders.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import org.example.microrders.BaseEntity;

/**
 *
 * @author markus-lauer
 */
@Entity
public class ServiceTechnician extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false, unique = true)
    private String name;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ServiceTechnician{" + "name=" + name + '}';
    }
    
}
