package org.example.microrders.order.entity;

/**
 *
 * @author markus-lauer
 */
public enum OrderStatus {
    CREATED, STARTED, FINISHED, FAILED
}
