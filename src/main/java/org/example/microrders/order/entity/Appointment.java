package org.example.microrders.order.entity;

import java.time.Duration;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.example.microrders.BaseEntity;

/**
 *
 * @author markus-lauer
 */
@Entity
public class Appointment extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @NotNull
    @Column(nullable = false)
    private LocalDateTime pointInTime;

    private Duration expectedDuration;
    
    @ManyToOne
    private ServiceTechnician serviceTechnician;

    @ManyToOne
    private Order order;

    public LocalDateTime getPointInTime() {
        return pointInTime;
    }

    public void setPointInTime(LocalDateTime pointInTime) {
        this.pointInTime = pointInTime;
    }

    public Duration getExpectedDuration() {
        return expectedDuration;
    }

    public void setExpectedDuration(Duration expectedDuration) {
        this.expectedDuration = expectedDuration;
    }

    public ServiceTechnician getServiceTechnician() {
        return serviceTechnician;
    }

    public void setServiceTechnician(ServiceTechnician serviceTechnician) {
        this.serviceTechnician = serviceTechnician;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Appointment{" + "pointInTime=" + pointInTime + ", serviceTechnician=" + serviceTechnician + ", order=" + order + '}';
    }
    
}
