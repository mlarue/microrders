/**
 * Author:  non
 * Created: 15.01.2017
 */

create table "order" (id uuid not null, orderId varchar(255) not null, status varchar(255) not null, primary key (id));
alter table "order" add constraint UK_nd0k9a0fot1c1opca6bo7dyj8 unique (orderId);
