/**
 * Author:  non
 * Created: 15.01.2017
 */

create table "administration"."tenant" (id uuid not null, name varchar(255) not null, primary key (id));
create table "administration"."user" (id uuid not null, name varchar(255) not null, password varchar(255), "tenant_id" uuid not null, primary key (id));
create table "administration"."user_role" (id binary(255) not null, role varchar(255) not null, "user_id" uuid not null, primary key (id));
alter table "administration"."tenant" add constraint UK_dcxf3ksi0gyn1tieeq0id96lm unique (name);
alter table "administration"."user" add constraint UK_gj2fy3dcix7ph7k8684gka40c unique (name);
alter table "administration"."user_role" add constraint UK3l4gwuhqpfj2yif7ae2n5vdkc unique ("user_id", role);
alter table "administration"."user" add constraint FKksltimyuv2eka0esgpew3qid6 foreign key ("tenant_id") references "administration"."tenant";
alter table "administration"."user_role" add constraint FKhjx9nk20h4mo745tdqj8t8n9d foreign key ("user_id") references "administration"."user";
